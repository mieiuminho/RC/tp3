# Domínios de colisão

## 17)

> **Faça ping de n1 para n2. Verifique com a opção tcpdump como flui o tráfego nas
diversas interfaces dos vários dispositivos. Que conclui?**

![Topologia do CORE com `hub`](figures/tp3model.png){#fig:tp3model width=450px}

![Resultado de `ping 10.0.0.10`](figures/pingN1_N2.png){#fig:pingN1_N2}

![Resultado de `tcpdump` em `n1`](figures/tcpdumpN1.png){#fig:tcpdumpN1 width=400px}

![Resultado de `tcpdump` em `n2`](figures/tcpdumpN2.png){#fig:tcpdumpN2 width=400px}

![Resultado de `tcpdump` em `n3`](figures/tcpdumpN3.png){#fig:tcpdumpN3 width=400px}

Depois de fazermos o ping de `n1` para `n2` percebemos que pedidos de `n1`
chegam a `n2` e `n2` comunica a resposta. 

Realizando o comando `tcpdump` em `n1`, `n2` e `n3` percebemos que os pedidos
de `n1` alcançam `n2` e `n3` pelo facto de o _hub_ não conseguir armazenar
endereços MAC e, por isso, envia os pacotes para todos os hosts.

## 18)

> **Na topologia de rede substitua o hub por um switch. Repita os procedimentos
que realizou na pergunta anterior. Comente os resultados obtidos quanto à
utilização de hubs e switches no contexto de controlar ou dividir domínios de
colisão. Documente as suas observações e conclusões com base no tráfego
observado/capturado.**

![Topologia do CORE com `switch`](figures/tp3model_switch.png){#fig:tp3model_switch width=450px}

![Resultado de `tcpdump` em `n1`](figures/tcpdumpN1_s.png){#fig:tcpdumpN1_s width=400px}

![Resultado de `tcpdump` em `n2`](figures/tcpdumpN2_s.png){#fig:tcpdumpN2_s width=400px}

![Resultado de `tcpdump` em `n3`](figures/tcpdumpN3_s.png){#fig:tcpdumpN3_s width=400px}

Constatamos que apenas o primeiro pedido de `n1` para `n2` alcança `n3`,
uma vez que o _switch_ tem capacidade de guardar endereços MAC de hosts
usados anteriormente, pelo que a partir do segundo pacote apenas o direciona
a `n2`.
