# Captura e análise de tramas Ethernet

![](figures/mac_and_type.png){ #fig:mac_and_type }

## 1)

> **Anote os endereços MAC de origem e de destino da trama capturada.**

Os endereços de MAC de origem (azul claro) e de destino (azul escuro) podem ser
consultados na @fig:mac_and_type. Assim o endereço MAC de origem é:
`50:7b:9d:83:bc:1a` enquanto que o endereço MAC destino é `00:0c:29:d2:19:f0`.

## 2)

> **Identifique a que sistemas se referem. Justifique.**

O endereço MAC de origem refere-se à interface ethernet da máquina usada para o
trabalho prático. O endereço MAC destino corresponde à interface do router da
rede local (que se encarrega depois de encaminhar a trama para o servidor).
Conforme já constatamos nos trabalhos práticos anteriores, uma máquina numa rede
local não conhece endereços exteriores, pelo que o endereço destino tem de ser o
da interface do router local.

## 3)

> **Qual o valor hexadecimal do campo Type da trama Ethernet? O que significa?**

O valor é `0x0800` (amarelo). Indica que a trama encapsula um pacote IPv4.

## 4)

> **Quantos bytes são usados desde o início da trama até ao caractere ASCII “G” do
método HTTP  GET?  Calcule  e  indique,  em  percentagem,  a  sobrecarga
(_overhead_)  introduzida pela pilha protocolar no envio do HTTP GET.**

![Overhead](figures/overhead.png){#fig:overhead}

O tamanho total da trama ethernet em bytes é 426. São usados 66 bytes da trama
até ao caratere ASCII "G" do método `HTTP GET`, por isso a percentagem de _overhead_
pode ser calculada como $(66 / 426) * 100 = 15.49$. Assim, a percentagem de _overhead_
da trama ethernet é 15.49%.

## 5)

> **Através de visualização direta de uma trama capturada, verifique que,
possivelmente, o campo FCS (_Frame Check Sequence_) usado para deteção de erros
não está a ser usado. Em sua opinião, porque será?**

Não se usa o _Frame Check Sequence_ em rede Ethernet uma vez que é muito pouco
suscetível a erros e por isso não se justifica o uso de um campo para correção
de erros.

## 6)

> **Qual é o endereço Ethernet da fonte? A que sistema de rede corresponde?
Justifique.**

![Fonte e destino da resposta HTTP](figures/source_destination_response.png){#fig:overhead}

O endereço Ethernet da fonte é `00:0c:29:d2:19:f0`. Este endereço corresponde
ao router da rede local à qual nos conectamos (o gateway da nossa rede local),
como se pode verificar na @fig:overhead.

## 7)

> **Qual é o endereço MAC do destino? A que sistema corresponde?**

O endereço MAC do destino é `50:7b:9d:83:bc:1a`. É o endereço MAC da nossa
máquina, como se pode verificar na @fig:overhead.

## 8)

> **Atendendo  ao  conceito  de  desencapsulamento  protocolar,  identifique  os
vários protocolos contidos na trama recebida.**

HTTP, TCP e IPv4

