# Conclusão {#sec:end}

A realização deste trabalho prático foi extremamente elucidativa,
contribuindo para um melhor entendimento da deteção de erros em
tramas Ethernet, endereços MAC, interligação de redes locais, do
protocolo ARP e dos domínios de colisão de redes.

Pudemos analisar diretamente as tramas Ethernet, constatando que
estas contêm um _overhead_ de _bytes_ em relação ao conteúdo a
transportar. Constatamos que as tramas Ethernet não têm FCS
(_Frame Check Sequence_) uma vez que as transmissões Ethernet estão
muito pouco sujeitas a erros, razão pela qual seria desnecessário
incluir o segmento FCS.

Como consequência deste trabalho possuímos agora conhecimento prático
também relativamente a mecanismos de mapeamento entre os endereços de rede através do manuseamento do protocolo _ARP_.