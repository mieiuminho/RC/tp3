# Protocolo ARP

## 9)

> **Observe o conteúdo da tabela ARP. Explique o significado de cada uma das
colunas.**

![Output `arp -a`](figures/arp_a.png){#fig:arp_a}

A primeira coluna da tabela contém o endereço IP do host, a segunda coluna
tem o endereço MAC do host e a terceira a interface.

## 10)

> **Qual  é  o  valor  hexadecimal  dos  endereços  origem  e  destino  na  trama
Ethernet  que contém  a  mensagem  com  o  pedido  ARP  (ARP Request)?  Como
interpreta  e  justifica  o endereço destino usado?**

![Origem, Destino e Valor do Campo do tipo da trama](figures/arp_request.png){#fig:arp_request}

O valor hexadecimal do endereço origem é: `50:7b:9d:83:bc:1a`, enquanto que o
valor hexadecimal do endereço destino é: `ff:ff:ff:ff:ff:ff`, como se verifica na @fig:arp_request.
Como a origem da trama Ethernet não sabe o endereço MAC do destino, envia a
trama para todos os hosts possíveis, sendo que o host que tiver o IP destino
responde com o respetivo endereço MAC[^1].


[^1]: Usando a trama ethernet que contém a mensagem com o pedido ARP gerado a partir do comando `ping -w 2 192.168.100.195`.

## 11)

> **Qual o valor hexadecimal do campo tipo da trama Ethernet? O que indica?**

O valor em hexadecimal é `0x0806`, indicando o protocolo ARP, como se verifica na
@fig:arp_request.

## 12)

> **Qual o valor do campo ARP opcode? O que especifica?  Se necessário, consulte a
RFC do protocolo ARP (<http://tools.ietf.org/html/rfc826.html>).**

![Trama ARP](figures/arpfield_request.png){#fig:arpfield}

O valor do campo ARP opcode é 1, significando que se trata de um pedido, como se
pode constatar na @fig:arpfield, a azul.

## 13)

> **Identifique que tipo de endereços está contido na mensagem ARP? Que conclui?**

Endereços MAC e endereços IP: endereço MAC da fonte, endereço IP da fonte, endereço MAC
do destino e endereço IP do destino, como se pode verificar na @fig:arpfield, a
verde claro.

## 14)

> **Explicite que tipo de pedido ou pergunta é feito pelo host de origem?**

Pergunta qual o endereço MAC que tem como endereço IP o endereço IP destino
(192.168.100.195).

## 15)

> **Localize a mensagem ARP que é a resposta ao pedido ARP efectuado.**

![Tramas ARP](figures/arps.png){#fig:arps}

### a)

> **Qual o valor do campo ARP opcode? O que especifica?**

![Trama ARP da resposta](figures/arp_reply.png){#fig:arp_reply}

O valor do cammpo ARP opcode é 2, significando que se trata de uma resposta,
como se pode verificar na @fig:arp_reply, a azul claro.

### b)

> **Em que posição da mensagem ARP está a resposta ao pedido ARP?**

A resposta ao pedido ARP encontra-se no campo _Sender MAC address_ da resposta,
sendo que a resposta é: `68:f7:28:81:40:a0`, como se pode verificar na
@fig:arp_reply, a verde claro.

## 16)

> **Identifique um pacote de pedido ARP gratuito originado pelo seu sistema.
Analise o conteúdo de u pedido ARP gratuito e identifique em que se distingue
dos restantes pedidos ARP. Registe a trama Ethernet correspondente. Qual o
resultado esperado face ao pedido ARP gratuito enviado?**

![ARP Gratuito](figures/gratuito.png){#fig:gratuito}

 O ARP gratuito distingue-se dos outros pelo facto de ter: [Is gratuituos: True].
 O resultado esperado para este é pedido é que como consequência do mesmo, todos
 os hosts na rede passem a ter o endereço MAC do host que fez o pedido.
